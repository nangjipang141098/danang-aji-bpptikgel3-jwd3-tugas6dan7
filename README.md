# Penlilaian Pelatihan
Ini adalah ReadMe yang dibuat untuk aplikasi input nilai peserta pelatihan. Aplikasi ini dibuat oleh Danang Aji Pangestu untuk memenuhi nilai Tugas 7 JWD3 - BPPTIK GEL-3 2020.  Aplikasi input ini sebelumnya telah dibuat pada Tugas 6. 

## Fitur
Aplikasi ini memiliki beberapa fitur yang dapat digunakan, yaitu sebagai berikut.
1. Dapat menginput empat jenis nilai.
2. Dapat mengetahui hasiil nilai total.

## Build With
Aplikasi ini dibuat menggunakan :
* [PHP](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwi7jf3Y-IPqAhXTbX0KHRv-C2UQFjALegQIEhAB&url=https%3A%2F%2Fid.wikipedia.org%2Fwiki%2FPHP&usg=AOvVaw38Ia_GQ-zshcsTZYO7vHLY) - Bahasa Pemrograman
* [CSS](https://www.w3schools.com/css/css_intro.asp) - Markup
* [Visual Studio Code](https://code.visualstudio.com/) - Aplikasi Text Editor

## Credits
* [Danang Aji Pangestu](https://gitlab.com/nangjipang141098/danang-aji-bpptikgel3-jwd3-tugas6dan7)
* JWD3 - BPPTIK 2020